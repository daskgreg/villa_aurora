import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  theName:any;
  theEmail:any;
  thePassword:any;
  theEmailLogin:any;
  thePasswordLogin:any;
  yolo:any;

  constructor(private http:HttpClient) {}


  sendPostRequest() {
   
   
    let postData = {
            "name": this.theName,
            "email": this.theEmail,
            "password": this.thePassword
    }

    this.http.post("http://localhost:3000/api/user/register", postData, {headers : new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
    })})
      .subscribe(data => {
        console.log(data);
       }, error => {
        console.log(error);
      });
  }
  sendPostRequestLogin() {
   
   
    let postData = {
            "email": this.theEmailLogin,
            "password": this.thePasswordLogin
    }

    this.http.post("http://localhost:3000/api/user/login", postData, {responseType:'text'}
    )
      .subscribe(data => {
       console.log(data);
      });
  }

  getData(){

    console.log('before request');

    this.http.get('http://localhost:3000/api/posts',{headers: new HttpHeaders({'auth-token':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDdiNTAyOTQyNjVjZDFmNGQxOGViMzEiLCJpYXQiOjE2MTg2OTUwOTJ9.sbZkj9WBLC1vDBARqJ38caxTX2DCkybhe8pWKOVDJp4'})}).subscribe(data => {
      console.log(data);
    });

  }
}
